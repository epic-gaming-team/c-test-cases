#include <stdio.h>

int main(){

  int num1, num2;
  int add;

  printf("Enter two numbers and hit enter:");
  scanf("%d%d", &num1, &num2);

  add = num1 + num2;
  
  printf("The sum is %d\n", add);

  return 0;}
